package com.example.hakaton1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnTextChanged;

public class MainActivity extends AppCompatActivity {


    @BindView(R.id.fa_sumContribut)
    EditText sumContribut;
    @BindView(R.id.fa_term)
    EditText terminContribut;
    @BindView(R.id.fa_addingInMonth)
    EditText addingInMonth;
    @BindView(R.id.fa_bet)
    EditText yyyyy;
    @BindView(R.id.fa_tax)
    EditText textTax;
    @BindView(R.id.textView)
    TextView textView;
    @BindView(R.id.fa_monthIncome)
    TextView monthIncome;
    @BindView(R.id.fa_sumIncome)
    TextView sumIncome;
    @BindView(R.id.fa_sumRefill)
    TextView sumRefill;
    @BindView(R.id.fa_sumTotal)
    TextView sumTotal;
    @BindView(R.id.checkBox)
    CheckBox checkBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);




    }

    @OnTextChanged({R.id.fa_sumIncome, R.id.fa_term, R.id.fa_addingInMonth, R.id.fa_bet, R.id.fa_tax, R.id.checkBox})
    public void depositing()    {
        double sumDeposit = Double.valueOf(sumContribut.getText().toString());
        int term = Integer.valueOf(terminContribut.getText().toString());
        double addSumInMonth = Double.valueOf(addingInMonth.getText().toString());
        double bet = Double.valueOf(yyyyy.getText().toString());
        double tax = Double.valueOf(textTax.getText().toString());

        Deposit deposit1 = new Deposit(sumDeposit, term, addSumInMonth, bet, tax, checkBox.isChecked());
    }







}
